package in.utk4rsh.java.design.patterns.adapter;

public interface Duck {

	public void quack();

	public void fly();
}
