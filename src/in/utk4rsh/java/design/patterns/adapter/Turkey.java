package in.utk4rsh.java.design.patterns.adapter;

public interface Turkey {
	
	public void gobble();

	public void fly();
}