package in.utk4rsh.java.design.patterns.singleton;

public enum SingletonEnum {
	INSTANCE;

	@Override
	public String toString() {
		return getDeclaringClass().getCanonicalName() + "@" + hashCode();
	}
}
