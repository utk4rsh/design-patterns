package in.utk4rsh.java.design.patterns.singleton;

public class SingletonThreadsafe {
	private static SingletonThreadsafe INSTANCE;

	/**
	 * private constructor to disallow construction of object from outside the
	 * class
	 */
	private SingletonThreadsafe() {
	}

	/**
	 * Static method with lazy initialization of instance.
	 * 
	 * @return
	 */
	public static synchronized SingletonThreadsafe getSingletonInstance() {
		if (INSTANCE == null) {
			INSTANCE = new SingletonThreadsafe();
		}
		return INSTANCE;
	}

	public String getDescription() {
		return "Singleton implementation with lazy initialization. This implementation is thread safe";
	}
}
