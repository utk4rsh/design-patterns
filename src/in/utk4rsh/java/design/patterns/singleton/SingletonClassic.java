package in.utk4rsh.java.design.patterns.singleton;

/**
 * 
 * @author utkarsh
 */
public class SingletonClassic {

	private static SingletonClassic INSTANCE;

	/**
	 * private constructor to disallow construction of object from outside the
	 * class
	 */
	private SingletonClassic() {
	}

	/**
	 * Static method with lazy initialization of instance.
	 * 
	 * @return
	 */
	public static SingletonClassic getSingletonInstance() {
		if (INSTANCE == null) {
			INSTANCE = new SingletonClassic();
		}
		return INSTANCE;
	}

	public String getDescription() {
		return "Classic Singleton implementation with lazy initialization. This implementation is not thread safe";
	}
}
