package in.utk4rsh.java.design.patterns.singleton;

/**
 * 
 * @author utkarsh
 *
 */
public class SingletonThreadSafeDoubleCheck {
	/**
	 * Volatile is used for reliable communication across threads. Volatile
	 * keyword solves the in the scenario when Thread A may assign a memory
	 * space for instance before it is finished constructing instance. Thread B
	 * will see that assignment and try to use it. This results in Thread B
	 * failing because it is using a partially constructed version of instance.
	 */
	private volatile static SingletonThreadSafeDoubleCheck INSTANCE;

	/**
	 * private constructor to disallow construction of object from outside the
	 * class
	 */
	private SingletonThreadSafeDoubleCheck() {
	}

	/**
	 * Static method with lazy initialization of instance.
	 * 
	 * @return
	 */
	public static SingletonThreadSafeDoubleCheck getSingletonInstance() {
		/**
		 * Local variable increases performance by 25 percent Joshua Bloch
		 * "Effective Java, Second Edition", p. 283-284
		 */
		SingletonThreadSafeDoubleCheck result = INSTANCE;
		if (result == null) {
			synchronized (SingletonThreadSafeDoubleCheck.class) {
				result = INSTANCE;
				if (result == null) {
					INSTANCE = result = new SingletonThreadSafeDoubleCheck();
				}
			}
		}
		return result;
	}

	public String getDescription() {
		return "Double check Singleton implementation with lazy initialization. This implementation is thread safe";
	}
}
